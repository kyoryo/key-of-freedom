﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace App.Domain.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public virtual UserProfile UserProfile { get; set; }
    }
}
