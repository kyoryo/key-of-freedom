﻿using System;

namespace App.Domain.Models.Core
{
    public class Product : IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public int Type { get; set; }
        public float Weight { get; set; }
        public float Height { get; set; }
        public float Distance { get; set; }
        public string Origin { get; set; }
        public string Destiation { get; set; }
    }
}
