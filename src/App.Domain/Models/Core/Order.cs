﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using App.Domain.Models.Identity;

namespace App.Domain.Models.Core
{
    public class Order : IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public string OrderNote { get; set; }
        public string Address { get; set; }
        public string PackageType { get; set; }
        public string OrderStatus { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
