﻿namespace App.Domain
{
    public interface IBaseEntity<T>
    {
        T Id { get; set; }
    }
}
