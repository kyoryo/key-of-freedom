﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace App.Services
{
    public interface IService<T> where T : class 
    {
        T GetById(params object[] ids);

        IList<T> GetAll(params Expression<Func<T, object>>[] includes);

        T Add(T model);

        T Update(T model);

        T Delete(T model);
    }
}
