﻿using App.Data.Repository;
using App.Domain.Models.Identity;

namespace App.Services.Identity
{
    public class UserService : BaseService<ApplicationUser, IRepository<ApplicationUser>>, IUserService
    {
        public UserService(IRepository<ApplicationUser> repository) : base(repository)
        {
        }
    }
}
