﻿using App.Data.Repository;
using App.Domain.Models.Identity;

namespace App.Services.Identity
{
    public class UserProfileService : BaseService<UserProfile, IRepository<UserProfile>>, IUserProfileService
    {
        public UserProfileService(IRepository<UserProfile> repository) : base(repository)
        {
        }
    }
}
