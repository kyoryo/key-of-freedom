﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using App.Data.Repository;

namespace App.Services
{
    public class BaseService<T, TRepo> : IService<T> 
        where T : class
        where TRepo : IRepository<T>
    {
        protected readonly TRepo _repository;

        public BaseService(TRepo repository)
        {
            _repository = repository;
        }

        public virtual T Add(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Add(model);

            return model;
        }

        public virtual T Update(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Update(model);

            return model;
        }

        public virtual T Delete(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Delete(model);

            return model;
        }

        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] includes)
        {
            return _repository.GetAll(includes)
                .ToList();
        }

        public virtual T GetById(params object[] ids)
        {
            return _repository.GetSingle(ids);
        }
    }
}
