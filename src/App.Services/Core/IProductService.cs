﻿using App.Domain.Models.Core;

namespace App.Services.Core
{
    public interface IProductService : IService<Product>
    {
    }
}
