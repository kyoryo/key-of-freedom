﻿using App.Domain.Models.Core;

namespace App.Services.Core
{
    public interface IOrderService : IService<Order>
    {
    }
}
