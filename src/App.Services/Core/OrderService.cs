﻿using App.Data.Repository;
using App.Domain.Models.Core;

namespace App.Services.Core
{
    public class OrderService : BaseService<Order, IRepository<Order>>, IOrderService
    {
        public OrderService(IRepository<Order> repository) : base(repository)
        {
        }
    }
}
