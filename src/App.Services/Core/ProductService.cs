﻿using App.Data.Repository;
using App.Domain.Models.Core;

namespace App.Services.Core
{
    public class ProductService : BaseService<Product, IRepository<Product>>, IProductService
    {
        public ProductService(IRepository<Product> repository) : base(repository)
        {
        }
    }
}
