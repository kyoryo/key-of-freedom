﻿using App.Data.Repository;
using App.Domain.Models.Core;

namespace App.Services.Core
{
    public class OrderProductService : BaseService<OrderProduct, IRepository<OrderProduct>>, IOrderProductService
    {
        public OrderProductService(IRepository<OrderProduct> repository) : base(repository)
        {
        }
    }
}
